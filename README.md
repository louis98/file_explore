# Why this readme exists
It is useful to accompany a repository project with a readme that contains a description on the purpose of a project, notes from contributors and general information about contents. Or just whatever you want it to be. 

# Things to do:
1) Navigate to T:\t-drive\Internal\Data\Data_team\DataLab\Git and create a folder with your name. 


# Some resources on Git:

An overview on git:
https://www.atlassian.com/git/tutorials/why-git
https://www.quickscrum.com/Article/ArticleDetails/5181/1/What-is-Git-What-benefits-does-Git-offer


A git command cheat sheet:
https://www.atlassian.com/git/tutorials/atlassian-git-cheatsheet

A video on what git is and how it works:
https://www.youtube.com/watch?v=8KCQe9Pm1kg

A git workflow model, GitFlow:
https://danielkummer.github.io/git-flow-cheatsheet/
http://nvie.com/posts/a-successful-git-branching-model/


# The .gitignore file
This is a cool feature for when we're working in a project, but don't want to upload certain components [like sensitive data]


# Edit to be made to the Rmd
```{r summary, echo=TRUE}
# Look at the summary of a file
summary(file)
```
